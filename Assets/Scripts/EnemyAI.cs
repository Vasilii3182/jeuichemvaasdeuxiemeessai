﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyAI : MonoBehaviour
{
    private GameObject player;
    private SpawnManager spawnManager;
    private float speed;
    private Rigidbody enemyRb;
    private float attackRange = 5;
    private bool hasAttacked = false;

    public float health;
    public float maxHealth = 100;
    public Slider healthSlider;

    void Start()
    {
        health = maxHealth;
        player = GameObject.Find("Player");
        spawnManager = GameObject.Find("SpawnManager").GetComponent<SpawnManager>();
        speed = spawnManager.enemySpeed;
        enemyRb = gameObject.GetComponent<Rigidbody>();
        healthSlider.transform.rotation = Camera.main.transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        if ((player.transform.position - transform.position).magnitude > attackRange && !hasAttacked)
        {
            transform.position += (player.transform.position - transform.position).normalized * speed * Time.deltaTime;
        }
        else if (!hasAttacked)
        {
            StartCoroutine(Attack(player.transform.position));
        }
        
    }

    IEnumerator Attack(Vector3 target)
    {
        hasAttacked = true;
        Color originalColor = gameObject.GetComponent<MeshRenderer>().material.color;
        gameObject.GetComponent<MeshRenderer>().material.color = Color.blue;
        Debug.Log("Color should be changed");
        yield return new WaitForSeconds(2f);
        enemyRb.AddForce((target-transform.position).normalized * 250, ForceMode.Impulse);
        gameObject.GetComponent<MeshRenderer>().material.color = originalColor;
        yield return new WaitForSeconds(1f);
        hasAttacked = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("GAME OVER");
        }
        if (collision.gameObject.CompareTag("Bullet"))
        {
            Destroy(collision.gameObject);
            //later damage input will be : TakeDamage(collision.gameObject.damage)
            TakeDamage(20);
        }
    }

    void TakeDamage(int damageDealt)
    {
        health -= damageDealt;
        healthSlider.value = health / maxHealth;
        if (health <= 0)
        {
            enemyRb.constraints = RigidbodyConstraints.None;
            enemyRb.AddForce((transform.position - player.transform.position).normalized * (speed * 100), ForceMode.Impulse);
            spawnManager.enemyKillCount++;
            spawnManager.textKillCount.text = "Enemies Slained : " + spawnManager.enemyKillCount;
            StartCoroutine(Death());
        }
    }

    IEnumerator Death()
    {
        yield return new WaitForSeconds(2f);
        Destroy(gameObject);
    }
}
