﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SpawnManager : MonoBehaviour
{
    public GameObject enemyPrefab;
    public GameObject player;
    public TextMeshProUGUI textKillCount;
    public float enemySpeed;
    public int enemyKillCount;
    public bool isGameActive = false;
    public GameObject inGameInterface;
    public GameObject titleMenu;

    float xMin = -24;
    float xMax = 24;
    float zMin = -24;
    float zMax = 24;
    private int waveCount = 1;
    int enemiesCount;

    public void StartGame()
    {
        titleMenu.gameObject.SetActive(false);
        player.gameObject.SetActive(true);
        inGameInterface.gameObject.SetActive(true);
        isGameActive = true;
        enemyKillCount = 0;
        Instantiate(enemyPrefab, GenerateSpawnLocation(), enemyPrefab.transform.rotation);
    }

    void Update()
    {
        if (isGameActive)
        {
            enemiesCount = FindObjectsOfType<EnemyAI>().Length;
            if (enemiesCount == 0)
            {
                waveCount++;
                SpawnEnemyWave();
            }
        }
    }

    void SpawnEnemyWave()
    {
        for (int i = 0; i < waveCount; i++)
        {
            Instantiate(enemyPrefab, GenerateSpawnLocation(), enemyPrefab.transform.rotation);
        }
    }

    Vector3 GenerateSpawnLocation()
    {
        Vector3 spawnLocation;

        do
        {
            float xPos = Random.Range(xMin, xMax);
            float zPos = Random.Range(zMin, zMax);
            spawnLocation = new Vector3(xPos, 0.5f, zPos);
        } while (CheckVector(spawnLocation));
        return (spawnLocation);

    }

    bool CheckVector(Vector3 vectorToCheck)
    {
        if (Physics.Raycast(vectorToCheck, (player.transform.position - vectorToCheck), 9))
        {
            return (true);
        }
        else
            return false;
    }
}
