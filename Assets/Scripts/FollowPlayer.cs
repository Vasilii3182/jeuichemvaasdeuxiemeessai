﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public GameObject player;

    Vector3 cameraOffset;
    void Start()
    {
        cameraOffset = transform.position - player.transform.position;
    }

    void LateUpdate()
    {
        //camera follows the players movements
        transform.position = player.transform.position + cameraOffset;
    }
}
